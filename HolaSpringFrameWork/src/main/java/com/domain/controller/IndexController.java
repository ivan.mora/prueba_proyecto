package com.domain.controller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.domain.modelo.Alumno;
import com.domain.modelo.dao.AlumnoDao;
import com.domain.util.ConnectionManager;

import es.edu.alter.practica0.model.PiedraPapelTijeraFactory;

import org.springframework.ui.Model;
@Controller
public class IndexController {
@RequestMapping("/home")
public String goIndex() {
	return "Index";
}
@RequestMapping("/")
public String getPresentacion() {
	return "Presentacion";
}

/*@RequestMapping("/listado")
public String goListado(Model model) {
List<String> alumnos = new ArrayList<String>();
alumnos.add("Juan");
alumnos.add("Pedro");
alumnos.add("Jos�");
model.addAttribute("titulo", "Listado de alumnos");
model.addAttribute("profesor", "Gabriel Casas");
model.addAttribute("alumnos", alumnos);
return "Listado";
}*/
@RequestMapping("/listado")
public String leer(Model pModel) throws ClassNotFoundException, SQLException {

	List<com.domain.modelo.Model> lista = new ArrayList<com.domain.modelo.Model>();
	AlumnoDao alu= new AlumnoDao();
	lista=alu.leer(new Alumno());
	pModel.addAttribute("lista",lista);

	return "Listado";
}

@RequestMapping("/juego/{nombre}")
public String goPiedraPapelTijera(@PathVariable("nombre") String nombre,Model model) {
//se debera crear un arrayList con las opciones piedra, papel y tijera List opciones�.
//completar como correspondan en este punto existen 3 opciones y hay una que es la 
//que mas megusta
//enviar las opciones a trav�s de 
	List <PiedraPapelTijeraFactory> opciones = new ArrayList<PiedraPapelTijeraFactory>();
	//System.out.println("nombre=" + nombre);
	for (int i=1;i<6;i++)
	opciones.add(PiedraPapelTijeraFactory.getInstance(i));
	/*opciones.add("piedra");
	opciones.add("papel");
	opciones.add("tijera");
	opciones.add("lagarto");
	opciones.add("spock");*/
	model.addAttribute("nombre",nombre);
	model.addAttribute("opciones",opciones);
return "PiedraPapelTijera";
}

@RequestMapping("/resolverJuego")
public String goResultado(String nombre,@RequestParam(required=false) Integer selOpcion, Model model) {
 //debo obtener la comparaci�n
 //determinar si el jugador gano, perdi� o empato
 //se debe tener en cuenta que el ordenador elige una opci�n al azar
 //utilizar el patr�n factory
//enviar los resultados 
	
	PiedraPapelTijeraFactory computadora = PiedraPapelTijeraFactory.getInstance((int)(Math.random()*100%5+1));
	PiedraPapelTijeraFactory jugador = PiedraPapelTijeraFactory.getInstance(selOpcion.intValue());


jugador.comparar(computadora);
model.addAttribute("jugador",jugador);
model.addAttribute("computadora",computadora);
model.addAttribute("resultado",jugador.getDescripcionREsultado());

//System.out.println(jugador.getDescripcionREsultado());
return "Resultados";
}

}
