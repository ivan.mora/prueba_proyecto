package es.edu.alter.practica0.modelo;

import java.util.List;
import java.util.ArrayList;


public abstract class PiedraPapelTijeraFactory {

	public final static int PIEDRA=1;
	public final static int PAPEL=2;
    public final static int TIJERA=3;
    public final static int LAGARTO=4;
    public final static int SPOCK=5;
    
protected String descripcionResultado;
private static List <PiedraPapelTijeraFactory> elementos;
protected String nombre;
protected int numero;
//constructor
public PiedraPapelTijeraFactory(String pNom,int pNum) {
nombre=pNom;
numero=pNum;

}
public String getNombre() {
return nombre;	
	} 
public void setNombre (String nombre) {
this.nombre=nombre;	
}

public int getNumero() {
return numero;	
}

public void setNumero (int numero) {
this.numero=numero;	
}

public String getDescripcionREsultado() {
	return descripcionResultado;
}

//metodo de negocio
public abstract boolean isMe(int pNum);
public abstract int comparar(PiedraPapelTijeraFactory pPiedPapelTijera);

public static PiedraPapelTijeraFactory getInstance(int pNumero) {
	//el corazon del factory
	//el padre reconoce a todos sus hijos
	elementos = new ArrayList<PiedraPapelTijeraFactory>();	
	elementos.add(new Piedra());
	elementos.add(new Papel());
	elementos.add(new Tijera());
	//nuevos
	elementos.add(new Lagarto());
	elementos.add(new Spock());
	for (PiedraPapelTijeraFactory PiedraPapelTijeraFactory : elementos) {
		if (PiedraPapelTijeraFactory.isMe(pNumero))
			return PiedraPapelTijeraFactory;
	
	
	
}
	return null;
}
}
