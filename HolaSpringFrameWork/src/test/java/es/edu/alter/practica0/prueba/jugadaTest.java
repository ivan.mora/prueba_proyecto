package es.edu.alter.practica0.prueba;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.alter.practica0.model.Jugada;
import es.edu.alter.practica0.model.Jugador;
import es.edu.alter.practica0.model.Piedra;
import es.edu.alter.practica0.model.Spock;

class jugadaTest {
Jugada jugada=null;
	@BeforeEach
	void setUp() throws Exception {
		Jugador jug1= new Jugador(1,"Gabriel","papacho",new Piedra()); 
		Jugador jug2= new Jugador(2,"compu","pc",new Spock());
		jugada=new Jugada(1,new Date(),jug1,jug2);
	}

	@AfterEach
	void tearDown() throws Exception {
		jugada=null;
	}

	@Test
	void test() {
		//System.out.println(jugada.getDescripcionREsultado());
		assertEquals("piedra pierde con spock",jugada.getDescripcionDelresultado());
	} 

}
