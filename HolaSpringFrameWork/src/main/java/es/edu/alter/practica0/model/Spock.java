package es.edu.alter.practica0.model;

public class Spock extends PiedraPapelTijeraFactory {

	public Spock() {
		this("spock",SPOCK);
	}
	public Spock (String pNom,int pNum) {
		super(pNom,pNum);
			}
	public boolean isMe(int pNum) {
		return pNum==SPOCK;		
	}
	public int comparar (PiedraPapelTijeraFactory pPiedPapelTijera) {
			int resul=0;
			switch (pPiedPapelTijera.getNumero()) {
			case PAPEL:
			case LAGARTO:	
				resul=-1;
				this.descripcionResultado="spock pierde con "+ pPiedPapelTijera.getNombre();			
				break;
			case PIEDRA:
			case TIJERA:	
				resul=1;
				this.descripcionResultado="spock gana a "+ pPiedPapelTijera.getNombre();			
				break;
				default:
					resul=0;
					this.descripcionResultado="spock empata con "+ pPiedPapelTijera.getNombre();
					break;
					
			}
			return resul;
		}
		
}