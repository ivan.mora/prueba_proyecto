package es.edu.alter.practica0.modelo;

import java.sql.Date;

public class Autoria {
	private Date fecha;
	private int cantidadJugadas;



	public Autoria() {}

	public Autoria(Date fecha, int catidadJugadas) {
	this.fecha = fecha;
	this.cantidadJugadas = catidadJugadas;
	}



	public Date getFecha() {
	return fecha;
	}



	public void setFecha(Date fecha) {
	this.fecha = fecha;
	}



	public int getCatidadJugadas() {
	return cantidadJugadas;
	}



	public void setCatidadJugadas(int catidadJugadas) {
	this.cantidadJugadas = catidadJugadas;
	}
	
	public void contarDespuesDeLaJugada() {
	cantidadJugadas ++;
	}
}
