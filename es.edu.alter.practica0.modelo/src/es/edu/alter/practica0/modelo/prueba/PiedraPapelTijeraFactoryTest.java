package es.edu.alter.practica0.modelo.prueba;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.alter.practica0.modelo.Lagarto;
import es.edu.alter.practica0.modelo.Papel;
import es.edu.alter.practica0.modelo.Piedra;
import es.edu.alter.practica0.modelo.PiedraPapelTijeraFactory;
import es.edu.alter.practica0.modelo.Spock;
import es.edu.alter.practica0.modelo.Tijera;

class PiedraPapelTijeraFactoryTest {
	//1- lote pruebas
	PiedraPapelTijeraFactory piedra,papel,tijera,lagarto,spock;
	

	@BeforeEach
	void setUp() throws Exception {
		//se ejecuta antes de cada prueba
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
		//2 nuevos
		lagarto = new Lagarto();
		spock = new Spock();
	}

	@AfterEach
	void tearDown() throws Exception {
		//se ejecuta despues de cada prueba
		piedra = null;
		papel  = null;
		tijera = null;
		lagarto = null;
		spock = null;
	}

	@Test
	void testGetInstancePiedra() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
													  .getNombre()
													  .toLowerCase());
		
	}
	@Test
	void testGetInstancePapel() {
		assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
													  .getNombre()
													  .toLowerCase());		
	}

	@Test
	void testGetInstanceTijera() {
		assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
													  .getNombre()
													  .toLowerCase());		
	}
	
	@Test
	void testGetInstanceLagarto() {
		assertEquals("lagarto", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO)
													  .getNombre()
													  .toLowerCase());		
	}
	
	@Test
	void testGetInstanceSpock() {
		assertEquals("spock", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOCK)
													  .getNombre()
													  .toLowerCase());		
	}
	
	
	
	//papel
	@Test
	void testCompararPapelEmpataConPapel() {
		//TODO para mis queridos alumnos testCompararPapelEmpataConPapel
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel empata con papel", papel.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaASpock() {
		assertEquals(1, papel.comparar(spock));
		assertEquals("papel gana a spock", papel.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	
	@Test
	void testCompararPapelPierdeConLagarto() {
		assertEquals(-1, papel.comparar(lagarto));
		assertEquals("papel pierde con lagarto", papel.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	
		
	@Test
	void testCompararPapelGanaConPiedra() {
		
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel gana a piedra", papel.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	
	@Test
	void testCompararPapelPierdeConTijera() {
		
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel pierde con tijera", papel.getDescripcionREsultado()
													  .toLowerCase());	
	}
	
	
	//piedra
	@Test
	void testCompararPiedraPierdeConPapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra pierde con papel", piedra.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	
	@Test
	void testCompararPiedraGanaATijera() {
		
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra gana a tijera", piedra.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	@Test
	void testCompararPiedraEmpataConPiedra() {
	
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra empata con piedra", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}

	@Test
	void testCompararPiedraPierdeConSpock() {

		assertEquals(-1, piedra.comparar(spock));
		assertEquals("piedra pierde con spock", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaALagarto() {
		assertEquals(1, piedra.comparar(lagarto));
		assertEquals("piedra gana a lagarto", piedra.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	
	
	
	//tijera

	@Test
	void testCompararTijeraGanaAPapel() {
		
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera gana a papel", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeConPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera pierde con piedra", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraEmpataConTijera() {
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera empata con tijera", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraPierdeConSpock() {

		assertEquals(-1, tijera.comparar(spock));
		assertEquals("tijera pierde con spock", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaALagarto() {
		assertEquals(1, tijera.comparar(lagarto));
		assertEquals("tijera gana a lagarto", tijera.getDescripcionREsultado()
													  .toLowerCase());
		
	}
	
	//spock
	
	@Test
	void testCompararSpockGanaAPapel() {
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera gana a papel", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararSpockGanaAPiedra() {
		assertEquals(1, spock.comparar(piedra));
		assertEquals("spock gana a piedra", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockGanaATijera() {
		assertEquals(1, spock.comparar(tijera));
		assertEquals("spock gana a tijera", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockEmpataConSpock() {

		assertEquals(0, spock.comparar(spock));
		assertEquals("spock empata con spock", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararSpockPierdeConLagarto() {
		assertEquals(-1, spock.comparar(lagarto));
		assertEquals("spock pierde con lagarto", spock.getDescripcionREsultado()
													  .toLowerCase());
		
	}

	//lagarto
	
	@Test
	void testCompararLagartoGanaAPapel() {
		assertEquals(1, lagarto.comparar(papel));
		assertEquals("lagarto gana a papel", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararLagartoPierdeConPiedra() {
		assertEquals(-1, lagarto.comparar(piedra));
		assertEquals("lagarto pierde con piedra", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoPierdeConTijera() {
		assertEquals(-1, lagarto.comparar(tijera));
		assertEquals("lagarto pierde con tijera", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoGanaASpock() {

		assertEquals(1, lagarto.comparar(spock));
		assertEquals("lagarto gana a spock", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararLagartoEmpataConLagarto() {
		assertEquals(0, lagarto.comparar(lagarto));
		assertEquals("lagarto empata con lagarto", lagarto.getDescripcionREsultado()
													  .toLowerCase());
		
	}	
	
}

